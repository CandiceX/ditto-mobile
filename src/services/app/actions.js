export const AppActionTypes = {
  UPDATE_NEW_CHAT_MODAL_VISIBLE: 'UPDATE_NEW_CHAT_MODAL_VISIBLE',
  UPDATE_PUSH_NOTIFICATIONS: 'UPDATE_PUSH_NOTIFICATIONS'
}

export const updateNewChatModalVisible = status => ({
  type: AppActionTypes.UPDATE_NEW_CHAT_MODAL_VISIBLE,
  payload: status
})

export const updatePushNotifications = notifs => ({
  type: AppActionTypes.UPDATE_PUSH_NOTIFICATIONS,
  payload: notifs
})
