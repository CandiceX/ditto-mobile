import { Subject } from 'rxjs'

const debug = require('debug')('ditto:scenes:settings:service')

// TODO: Handle matrix account settings

class SettingsService {
  _action
  actions

  constructor () {
    this._action = new Subject(null)
    this.actions = {
    }
  }

  async init () {
  }

  getAction () {
    return this._action
  }

  setAction (action) {
    if (!Object.values(this.actions).includes(action.type)) throw Error('Unknown action type:', action)
    debug('Set action:', action)
    this._action.next({ type: action.type, payload: action.payload })
  }
}

const settingsService = new SettingsService()
export default settingsService
