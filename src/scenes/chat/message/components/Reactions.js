import { useRoute } from '@react-navigation/native'
import { Text, useTheme } from '@ui-kitten/components'
import React from 'react'
import styled from 'styled-components/native'

import auth from '../../../auth/auth'
import messages from '../messages'

const debug = require('debug')('ditto:scenes:chat:message:components:Reactions')

export default function Reactions ({ message }) {
  const theme = useTheme()
  const userId = auth.getUserId()
  const roomId = useRoute()?.params?.roomId

  const { reactions } = message
  const otherBubble = message.sender.id !== userId

  const addReaction = (key) => {
    messages.addReaction(message.id, key, roomId)
  }

  const removeReaction = (reactionsForKey, key) => {
    const curUserReaction = reactionsForKey.find(r => r.sender === userId)
    if (curUserReaction) {
      debug('user reaction ', curUserReaction)
      messages.removeReaction(curUserReaction.event_id, roomId, message)
    }
  }

  debug('reactions ', reactions)

  return (
    <Wrapper style={{ alignSelf: otherBubble ? 'flex-end' : 'flex-start' }}>
      {Object.keys(reactions).map(key => {
        const isSelected = reactions[key].filter(s => s.sender === userId).length > 0
        let selectedStyle = {}
        if (isSelected) {
          selectedStyle = {
            backgroundColor: theme['color-primary-active'],
            borderWidth: 1.6,
            borderColor: theme['color-primary-800']
          }
        }
        return (
          <ButtonWrapper key={key} onPress={() => isSelected ? removeReaction(reactions[key], key) : addReaction(key)}>
            <ButtonContent style={[{ backgroundColor: theme['color-basic-800'] }, isSelected ? selectedStyle : {}]}>
              <Text>{key}</Text>
              <Text style={{ fontSize: 16 }} category='s1'>&nbsp;{reactions[key].length}</Text>
            </ButtonContent>
          </ButtonWrapper>
        )
      })}
    </Wrapper>
  )
}

const Wrapper = styled.View`
  flex-direction: row;
  z-index: 2;
  margin-bottom: -28;
  margin-top: 8;
  flex-wrap: wrap;
`

const ButtonWrapper = styled.TouchableOpacity`
  width: 54;
  height: 25;
  justify-content: center;
  align-items: center;
  margin-bottom: 8;
`

const ButtonContent = styled.View`
  background-color: blue;
  width: 50;
  height: 30;
  padding-top: 2;
  border-radius: 30;
  justify-content: center;
  align-items: center;
  flex-direction: row;
`
