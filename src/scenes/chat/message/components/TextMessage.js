import withObservables from '@nozbe/with-observables'
import { useTheme } from '@ui-kitten/components'
import Color from 'color'
import React, { useMemo } from 'react'
import { Linking } from 'react-native'
import Html from 'react-native-render-html'
import striptags from 'striptags'
import styled from 'styled-components/native'

import { getNameColor, isEmoji } from '../../../../utilities/Misc'
import auth from '../../../auth/auth'
import messages from '../messages'
import Reactions from './Reactions'

// const debug = require('debug')('ditto:scene:chat:message:components:TextMessage')

function TextMessage ({ message, sender, prevSender, nextSender, onLongPress }) {
  const theme = useTheme()
  const userId = auth.getUserId()

  const onLinkPress = (e, link) => {
    if (link) {
      Linking.canOpenURL(link).then(() => {
        Linking.openURL(link)
      })
    }
  }

  const _onLongPress = () => onLongPress(message)

  const prevSenderSame = prevSender === sender.id
  const nextSenderSame = nextSender === sender.id
  const props = { prevSenderSame, nextSenderSame }

  const displayText = messages.getContent(message, sender, 'html')

  const htmlProps = {
    ...getHtmlStyles(theme),
    onLinkPress
  }

  const renderHtml = useMemo(() => (
    <Html html={displayText} {...htmlProps} />
  // eslint-disable-next-line react-hooks/exhaustive-deps
  ), [displayText])

  if (userId === sender.id) {
    return (
      <>
        {isEmoji(striptags(displayText)) ? (
          <Emoji isUser {...props}>
            {striptags(displayText)}
          </Emoji>
        ) : (
          <MyBubble
            {...props}
            underlayColor={theme['color-primary-700']}
            onLongPress={_onLongPress}
            delayLongPress={200}
            style={[{ backgroundColor: theme['color-primary-active'] }, message.reactions ? { marginBottom: 18 } : {}]}
          >
            <>
              {renderHtml}
              {message.reactions && <Reactions message={message} />}
            </>
          </MyBubble>
        )}

        {!prevSenderSame && (
          <SenderText isUser color={getNameColor(sender.id)}>
            {sender.name}
          </SenderText>
        )}
      </>
    )
  } else {
    const underlay = Color(theme['background-basic-color-2']).darken(0.2).hex()
    return (
      <>
        {isEmoji(striptags(displayText)) ? (
          <Emoji {...props}>{striptags(displayText)}</Emoji>
        ) : (
          <OtherBubble
            {...props}
            underlayColor={underlay}
            onLongPress={() => {}}
            style={[{ backgroundColor: theme['background-basic-color-2'] }, message.reactions ? { marginBottom: 18 } : {}]}
          >
            <>
              {renderHtml}
              {message.reactions && <Reactions message={message} />}
            </>
          </OtherBubble>
        )}

        {!prevSenderSame && (
          <SenderText color={getNameColor(message.sender.id)}>
            {sender.name}
          </SenderText>
        )}
      </>
    )
  }
}
const enhance = withObservables(['message'], ({ message }) => ({
  sender: message.sender
}))
export default enhance(TextMessage)

const getHtmlStyles = theme => {
  return {
    baseFontStyle: {
      color: theme['text-basic-color'],
      fontSize: 16,
      letterSpacing: 0.3,
      fontWeight: '400'
    },
    tagsStyles: {
      blockquote: {
        borderLeftColor: theme['color-danger-active'],
        borderLeftWidth: 3,
        paddingLeft: 10,
        marginVertical: 10,
        opacity: 0.8
      },
      p: {},
      a: {
        color: theme['color-info-200']
      }
    }
  }
}

const Emoji = styled.Text`
  font-size: 45;
  margin-left: 18;
  margin-right: 8;
  margin-top: 4;
  margin-bottom: 4;
  align-self: ${({ isUser }) => (isUser ? 'flex-end' : 'flex-start')};
`

const Bubble = styled.TouchableHighlight`
  padding-left: 14;
  padding-right: 14;
  padding-top: 8;
  padding-bottom: 8;

  margin-top: ${({ prevSenderSame }) => (prevSenderSame ? 2 : 2)};
  margin-bottom: ${({ nextSenderSame }) => (nextSenderSame ? 1 : 4)};
  margin-left: 8;
  margin-right: 8;

  max-width: 300;

  border-radius: 18;
`

const sharpBorderRadius = 5

const OtherBubble = styled(Bubble)`
  align-self: flex-start;

  ${({ prevSenderSame }) =>
    prevSenderSame ? `border-top-left-radius: ${sharpBorderRadius};` : ''}
  ${({ nextSenderSame }) =>
    nextSenderSame ? `border-bottom-left-radius: ${sharpBorderRadius};` : ''}
`

const MyBubble = styled(Bubble)`
  background-color: blue;
  align-self: flex-end;
  z-index: 1;

  ${({ prevSenderSame }) =>
    prevSenderSame ? `border-top-right-radius: ${sharpBorderRadius};` : ''}
  ${({ nextSenderSame }) =>
    nextSenderSame ? `border-bottom-right-radius: ${sharpBorderRadius};` : ''}
`

const SenderText = styled.Text`
  color: ${({ color }) => (color || 'pink')};
  font-size: 14;
  font-weight: 400;
  margin-left: 22;
  margin-right: 22;
  margin-top: 8;
  opacity: 0.8;
  ${({ isUser }) => (isUser ? 'text-align: right;' : '')};
`
