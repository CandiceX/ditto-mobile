import { Layout } from '@ui-kitten/components'
import React from 'react'
import { StyleSheet, View } from 'react-native'

export default function BottomSheetHeader () {
  return (
    <Layout level='2' style={styles.layout}>
      <View style={styles.handle} />
    </Layout>
  )
}

const styles = StyleSheet.create({
  layout: {
    height: 30,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    alignItems: 'center'
  },
  handle: {
    borderRadius: 12,
    backgroundColor: 'gray',
    width: 50,
    height: 6,
    marginTop: 8
  }
})
