import { useNavigation } from '@react-navigation/native'
import { Layout, useTheme } from '@ui-kitten/components'
import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'

import { SCREEN_WIDTH } from '../../../../constants'

export default function BottomSheetContent () {
  const theme = useTheme()
  const navigation = useNavigation()

  const closeSheet = () => {
    navigation.goBack()
  }

  return (
    <Layout level='2' style={{ height: 600 }}>
      <View style={styles(theme).emojiRow}>
        <EmojiButton onPress={closeSheet} />
        <EmojiButton onPress={closeSheet} />
        <EmojiButton onPress={closeSheet} />
        <EmojiButton onPress={closeSheet} />
      </View>
    </Layout>
  )
}

function EmojiButton ({ onPress }) {
  const theme = useTheme()
  return (
    <TouchableOpacity style={styles(theme).emojiButton} onPress={onPress}>
      <Text style={{ fontSize: 24 }}>😊</Text>
    </TouchableOpacity>
  )
}

const styles = theme => StyleSheet.create({
  emojiRow: {
    flexDirection: 'row'
    // backgroundColor: 'blue'
  },
  emojiButton: {
    backgroundColor: theme['background-basic-color-1'],
    borderRadius: 50,
    width: SCREEN_WIDTH / 8,
    height: SCREEN_WIDTH / 8,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 12
  }
})
