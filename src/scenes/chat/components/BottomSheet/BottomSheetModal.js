import React, { useEffect, useRef } from 'react'
import { StyleSheet, TouchableWithoutFeedback, View } from 'react-native'
import BottomSheet from 'reanimated-bottom-sheet'

import BottomSheetContent from './BottomSheetContent'
import BottomSheetHeader from './BottomSheetHeader'

export default function BottomSheetModal ({ navigation }) {
  const bottomSheetRef = useRef()

  const renderBottomSheetContent = () => {
    return <BottomSheetContent />
  }

  const renderBottomSheetHeader = () => {
    return <BottomSheetHeader />
  }

  const closeModal = () => {
    navigation.goBack()
  }

  useEffect(() => {
    bottomSheetRef.current.snapTo(0)
  }, [bottomSheetRef])

  return (
    <TouchableWithoutFeedback onPress={closeModal}>
      <View style={styles.overlay}>
        <BottomSheet
          ref={bottomSheetRef}
          snapPoints={[300, 0]}
          initialSnap={1}
          renderContent={renderBottomSheetContent}
          renderHeader={renderBottomSheetHeader}
          onCloseEnd={closeModal}
        />
      </View>
    </TouchableWithoutFeedback>
  )
}

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: '#00000070',
    flex: 1
    // justifyContent: 'flex-end'
  }
})
