import withObservables from '@nozbe/with-observables'
import { useFocusEffect, useNavigation } from '@react-navigation/native'
import { useObservableState } from 'observable-hooks'
import React, { useCallback, useEffect, useState } from 'react'
import { FlatList } from 'react-native'

import auth from '../../auth/auth'
import chats from '../chats'
import Message from '../message/Message'
import messages from '../message/messages'

// const debug = require('debug')('ditto:scenes:chat:components:Timeline')

function Timeline ({ chat, messageList }) {
  const typing = useObservableState(chats.getTyping(chat.id))
  const [sortedMessages, setSortedMessages] = useState([])

  const navigation = useNavigation()

  const handleEndReached = async () => {
    if (!chat.atStart) chats.fetchPreviousMessages(chat.id)
  }

  const onLongPress = (message) => {
    navigation.navigate('BottomSheet')
  }

  useEffect(() => {
    let sortedMessages = []
    if (messageList) sortedMessages = messages.sortByLastSent(messageList)

    if (typing.length > 0 &&
        !(typing.length === 1 && typing[0] === auth.getUserId())) {
      sortedMessages.unshift({ id: 'typing', type: 'typing' })
    }

    setSortedMessages(sortedMessages)
  }, [messageList, typing])

  useEffect(() => {
    chats.chatOpened(chat.id)

    return chats.chatClosed(chat.id)
  }, [chat.id])

  const onSceneFocus = useCallback(() => {
    let message = null
    if (sortedMessages.length > 0) {
      if (sortedMessages.length > 1 && sortedMessages[0].id === 'typing') {
        message = sortedMessages[1]
      } else message = sortedMessages[0]

      const latestMessage = messages.getLatestMessage(chat.id)
      if (latestMessage.id !== message.id && message.sender.id !== auth.getUserId()) {
        chats.sendReadReceipt(chat.id, message.id)
      }
    }
  }, [chat.id, sortedMessages])
  useFocusEffect(onSceneFocus)

  return (
    <FlatList
      keyboardDismissMode='on-drag'
      keyboardShouldPersistTaps='handled'
      inverted
      data={sortedMessages}
      renderItem={({ item, index }) => (
        <Message
          message={item}
          prevSender={getPrevSender(sortedMessages, index)}
          nextSender={getNextSender(sortedMessages, index)}
          onLongPress={onLongPress}
        />
      )}
      onEndReached={handleEndReached}
      onEndReachedThreshold={0.5}
      keyExtractor={item => item.id}
    />
  )
}
const enhance = withObservables(['chat'], ({ chat }) => ({
  chat: chat,
  messageList: chat.messages
}))
export default enhance(Timeline)

const getPrevSender = (sortedMessages, index) => {
  if (!sortedMessages[index + 1]) return ''
  if (sortedMessages[index + 1]?.type === 'text') {
    return sortedMessages[index + 1]?.sender?.id
  }
  return getPrevSender(sortedMessages, index + 1)
}

const getNextSender = (sortedMessages, index) => {
  if (!sortedMessages[index - 1]) return ''
  if (sortedMessages[index - 1]?.type === 'text') {
    return sortedMessages[index - 1]?.sender?.id
  }
  return getPrevSender(sortedMessages, index - 1)
}
