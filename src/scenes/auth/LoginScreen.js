import { useNavigation } from '@react-navigation/native'
import { Button, Icon, Input, Layout, Spinner, Text, TopNavigation, useTheme } from '@ui-kitten/components'
import { useObservableState } from 'observable-hooks'
import React, { useEffect, useRef, useState } from 'react'
import { SafeAreaView, View } from 'react-native'
import Image from 'react-native-scalable-image'
import styled from 'styled-components/native'

import { SCREEN_HEIGHT, SCREEN_WIDTH } from '../../constants'
import { isIos } from '../../utilities/Misc'
import auth from './auth'

const topBlob = require('../../assets/images/blob4.png')
const bottomBlob = require('../../assets/images/blob5.png')

// const debug = require('debug')('ditto:scenes:auth:LoginScreen')

export default function LoginScreen () {
  const navigation = useNavigation()
  const passwordInput = useRef(null)
  const theme = useTheme()
  // State
  const [usernameValue, setUsernameValue] = useState('')
  const [passwordValue, setPasswordValue] = useState('')
  const [homeserverValue, setHomeserverValue] = useState('')
  const [errorText, setErrorText] = useState('')
  const [isLoading, setIsLoading] = useState(false)
  // Auth
  const authError = useObservableState(auth.getError())

  // ********************************************************************************
  // Methods
  // ********************************************************************************

  const handleLoginPress = () => {
    if (usernameValue.length === 0) {
      setErrorText('Missing username')
    } else if (passwordValue.length === 0) {
      setErrorText('Missing password')
    } else {
      auth.resetError()
      setErrorText('')
      setIsLoading(true)
      auth.login(usernameValue, passwordValue, homeserverValue)
    }
  }

  const handleBackPress = () => navigation.goBack()

  const renderBackButton = () => (
    <Button
      style={{ backgroundColor: 'transparent', borderColor: 'transparent', paddingLeft: 0 }}
      onPress={handleBackPress}
      icon={style => <Icon {...style} name={`arrow-${isIos() ? 'ios-' : ''}back`} width={35} height={35} />}
    />
  )

  // ********************************************************************************
  // Lifecycle
  // ********************************************************************************

  useEffect(() => {
    if (authError) {
      setIsLoading(false)
      setErrorText(authError)
    }
  }, [authError])

  useEffect(() => {
    setErrorText('')
  }, [usernameValue, passwordValue, homeserverValue])

  return (
    <Layout level='4' style={{ flex: 1 }}>
      <SafeAreaView>
        <TopNavigation title='Welcome back!' alignment={isIos() ? 'center' : 'start'} titleStyle={{ fontSize: 25 }} style={{ backgroundColor: 'transparent' }} leftControl={renderBackButton()} />
      </SafeAreaView>
      <TopBlob source={topBlob} />
      <BottomBlob source={bottomBlob} />

      <PageMargin keyboardShouldPersistTaps='handled'>
        <Input
          label='Username' size={isIos() ? 'large' : 'small'} placeholder='ex. john_smith' autoFocus
          value={usernameValue}
          onChangeText={setUsernameValue}
          returnKeyType='next'
          onSubmitEditing={() => passwordInput.current.focus()}
          autoCapitalize='none'
        />
        <Input
          label='Password' size={isIos() ? 'large' : 'small'} placeholder="Shh don't tell"
          style={{ marginTop: 15 }}
          ref={passwordInput}
          secureTextEntry
          value={passwordValue}
          onChangeText={setPasswordValue}
          autoCapitalize='none'
        />
        <View style={{ position: 'relative' }}>
          <Input
            disabled
            label='Homeserver' size={isIos() ? 'large' : 'small'} placeholder='matrix.org FAKE (optional)'
            style={{ marginTop: 15, position: 'absolute', width: '100%', borderColor: 'transparent' }}
            value='https://'
            onChangeText={() => {}}
            autoCapitalize='none'
            returnKeyType='go'
            onSubmitEditing={handleLoginPress}
          />
          <Input
            label=' ' size={isIos() ? 'large' : 'small'} placeholder='matrix.org (optional)'
            style={{ marginTop: 15, marginLeft: SCREEN_WIDTH * 0.175 }}
            value={homeserverValue}
            onChangeText={setHomeserverValue}
            autoCapitalize='none'
            returnKeyType='go'
            onSubmitEditing={handleLoginPress}
            textStyle={{ marginLeft: -3 }}
          />
        </View>

        <View style={{ marginTop: 40 }}>
          {isLoading ? (
            <Layout style={{ justifyContent: 'center', alignItems: 'center', height: 58, borderRadius: 50, backgroundColor: theme['color-info-500'] }}>
              <Spinner status='basic' />
            </Layout>
          ) : (
            <Button onPress={handleLoginPress} size='giant' status='info' style={{ borderRadius: 50 }}>
              Login
            </Button>
          )}
        </View>

        {errorText.length > 0 && (
          <Text status='danger' style={{ textAlign: 'center', marginTop: 15 }}>
            Looks like something went wrong:{`\n\n${errorText}\n\n`}Try again!
          </Text>
        )}
        <View style={{ height: SCREEN_HEIGHT / 2 }} />
      </PageMargin>
    </Layout>
  )
}

const PageMargin = styled.ScrollView`
  padding-left: 30;
  padding-right: 30;
  padding-top: 30;
`

const TopBlob = styled(Image)`
  position: absolute;
  top: -285;
  left: -100;
  z-index: -1;
`

const BottomBlob = styled(Image)`
  position: absolute;
  bottom: -500;
  left: -400;
`
