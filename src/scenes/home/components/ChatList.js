import { useNavigation } from '@react-navigation/native'
import {
  Button,
  Icon as EvaIcon,
  useTheme
} from '@ui-kitten/components'
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { Alert, Animated, FlatList, InteractionManager } from 'react-native'
import Swipeable from 'react-native-gesture-handler/Swipeable'
import styled from 'styled-components/native'

import chats from '../../chat/chats'
import ChatListItem from './ChatListItem'

const debug = require('debug')('ditto:scenes:home:components:ChatList')

export default function ChatList ({ chatList }) {
  const [swipableRefs, setSwipableRefs] = useState([])

  const theme = useTheme()
  const navigation = useNavigation()

  const recenterSwipeables = useCallback(() => {
    swipableRefs.forEach(ref => ref && ref.close())
  }, [swipableRefs])

  const navigateToChat = useCallback(chat => {
    InteractionManager.runAfterInteractions(() => {
      recenterSwipeables()
      navigation.navigate('Chat', {
        roomId: chat.id,
        roomTitle: chat.name
      })
    })
  }, [navigation, recenterSwipeables])

  const openLeftEditButtons = useCallback((index) => {
    recenterSwipeables()
    swipableRefs[index].openLeft()
  }, [recenterSwipeables, swipableRefs])

  const leaveChat = useCallback(async (chat) => {
    Alert.alert(
      'Leave Chat',
      `Are you sure you would like to leave "${chat.name}"?`,
      [
        {
          text: 'Stay',
          onPress: () => {}
        },
        {
          text: 'Leave',
          onPress: () => confirmLeaveChat(chat.id),
          style: 'destructive'
        }
      ],
      { cancelable: false }
    )
  }, [])

  const renderLeftActions = useCallback((progress, dragX, chat) => {
    const trans = dragX.interpolate({
      inputRange: [0, 40, 60, 80],
      outputRange: [-50, 0, 0, 0]
    })
    return (
      <Animated.View
        style={[{ transform: [{ translateX: trans }] }, { height: '100%', justifyContent: 'center', marginLeft: 12 }]}
      >
        <ActionButton
          onPress={() => leaveChat(chat)}
          appearance='filled'
          style={{ borderWidth: 0, backgroundColor: theme['background-basic-color-1'] }}
          icon={style => <EvaIcon {...style} name='trash' width={20} height={20} />}
        />
      </Animated.View>
    )
  }, [leaveChat, theme])

  const confirmLeaveChat = async (id) => {
    try {
      chats.leaveChat(id)
    } catch (e) {
      debug('leaveRoom error', e)
    }
  }

  const _renderItem = useCallback(({ item: chat, index }) => {
    const _onLongPress = () => openLeftEditButtons(index)
    const _onPress = () => navigateToChat(chat)
    return (
      <Swipeable enabled={false} ref={ref => { swipableRefs[index] = ref }} renderLeftActions={(p, d) => renderLeftActions(p, d, chat)} leftThreshold={100} overshootFriction={8}>
        <ChatListItem
          onLongPress={_onLongPress}
          onPress={_onPress}
          chat={chat}
        />
      </Swipeable>
    )
  }, [navigateToChat, openLeftEditButtons, renderLeftActions, swipableRefs])

  useEffect(() => {
    setSwipableRefs(Array(chatList.length).fill(null))
  }, [chatList])

  return useMemo(() => (
    <FlatList
      onScroll={recenterSwipeables}
      style={{ backgroundColor: 'transparent' }}
      data={chatList}
      renderItem={_renderItem}
    />
  ), [_renderItem, chatList, recenterSwipeables])
}

const ActionButton = styled(Button)`
  width: 25;
  height: 25;
  margin-right: 3;
  border-radius: 50;
  align-self: flex-end;
  justify-content: center;
  align-items: center;
`
