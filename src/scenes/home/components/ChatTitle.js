import withObservables from '@nozbe/with-observables'
import { Text } from '@ui-kitten/components'
import React, { useEffect, useState } from 'react'
import { of as of$ } from 'rxjs'
import styled from 'styled-components/native'

import { SCREEN_WIDTH } from '../../../constants'
import messages from '../../chat/message/messages'
import users from '../../user/users'

// const debug = require('debug')('ditto:scenes:home:components:ChatTitle')

function ChatTitle ({ chat, latestMessage, unread, sender, typing }) {
  const [snippet, setSnippet] = useState('')

  useEffect(() => {
    if (typing.length > 0) {
      const setTypingUsers = async () => {
        const userList = await users.getUsers(typing)
        setSnippet(messages.getContent({ type: 'typing', content: userList }))
      }
      setTypingUsers()
    } else if (latestMessage) setSnippet(messages.getContent(latestMessage, sender))
  }, [typing, latestMessage, sender])

  return (
    <StyledView>
      <Text category='p1' numberOfLines={1} style={unread ? { fontWeight: 'bold', fontSize: 15 } : {}}>{chat.name}</Text>
      <Text
        appearance='hint'
        category='p2'
        numberOfLines={2}
        style={unread ? { fontWeight: 'bold' } : {}}
      >
        {snippet}
      </Text>
    </StyledView>
  )
}
const enhance = withObservables(['latestMessage'], ({ latestMessage }) => ({
  sender: latestMessage ? latestMessage.sender : of$(null)
}))
export default enhance(ChatTitle)

const StyledView = styled.View`
  max-width: ${SCREEN_WIDTH * 0.5};
  margin-left: 14;
`
